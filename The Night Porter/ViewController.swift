//
//  ViewController.swift
//  The Night Porter
//
//  Created by Andrzej Lapinski on 12/11/2018.
//  Copyright © 2018 Andrzej Lapinski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func changeBackground(_ sender: Any) {
        view.backgroundColor = UIColor.darkGray
        
        // get everything contained in the top-level view
        let everything = view.subviews
        for eachView in everything {
            if eachView is UILabel {
                let currentLabel = eachView as! UILabel
                currentLabel.textColor = UIColor.lightGray
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

